<?php

namespace apstudio\assetsvideo\variables;

use apstudio\assetsvideo\AssetsVideo;

class AssetsVideoVariable{
  // Public Methods
  // =========================================================================

  public function getVideoUrl($filePath, $videoOptions): string{
    return AssetsVideo::$plugin->process->getVideoUrl($filePath, $videoOptions);
  }

  public function getVideoThumbnailUrl($filePath, $thumbnailOptions){
    return AssetsVideo::$plugin->process->getVideoThumbnailUrl($filePath, $thumbnailOptions);
  }

  public function getFileInfo($filePath, $summary = false): array{
    return AssetsVideo::$plugin->process->getFileInfo($filePath, $summary);
  }

  public function getGifUrl($filePath, $gifOptions): string{
    return AssetsVideo::$plugin->process->getGifUrl($filePath, $gifOptions);
  }
}
