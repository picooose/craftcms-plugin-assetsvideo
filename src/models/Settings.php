<?php

namespace apstudio\assetsvideo\models;

use craft\base\Model;
use craft\validators\ArrayValidator;

class Settings extends Model{

  // Static Methods
  // =========================================================================

  public function __construct(array $config = []){
    parent::__construct($config);
  }

  // Public Properties
  // =========================================================================

  public $ffmpegPath        = '/usr/bin/ffmpeg';
  public $ffprobePath       = '/usr/bin/ffprobe';
  public $ffprobeOptions    = '-v quiet -print_format json -show_format -show_streams';
  public $useHashedNames    = true;
  public $hashedNamesLength = 10;

  public $videoPaths = [
    'default'   => '@webroot/video/',
  ];

  public $videoUrls = [
    'default'   => '@web/video/',
  ];

  public $videoEncoders = [
    'h264' => [
      'fileSuffix' => '.mp4',
      'fileFormat' => 'mp4',
      'videoCodec' => 'libx264',
      'videoCodecOptions' => '-vprofile high -preset slow -crf 22',
      'audioCodec' => 'libfdk_aac',
      'audioCodecOptions' => '-async 1000',
    ],
    'webm' => [
      'fileSuffix' => '.webm',
      'fileFormat' => 'webm',
      'videoCodec' => 'libvpx',
      'videoCodecOptions' => '-quality good -cpu-used 0',
      'audioCodec' => 'libvorbis',
      'audioCodecOptions' => '-async 1000',
    ],
    'gif' => [
      'fileSuffix' => '.mp4',
      'fileFormat' => 'mp4',
      'videoCodec' => 'libx264',
      'videoCodecOptions' => '-pix_fmt yuv420p -movflags +faststart -filter:v crop=\'floor(in_w/2)*2:floor(in_h/2)*2\' ',
    ],
  ];

  public $defaultVideoOptions = [
    // Video settings
    'videoEncoder'    => 'h264',
    'videoBitRate'    => '800k',
    'videoFrameRate'  => 15,
    // Audio settings
    'audioBitRate'    => '',
    'audioSampleRate' => '',
    'audioChannels'   => '',
    // Spatial settings
    'width'           => '',
    'height'          => ''
  ];

  public $defaultThumbnailOptions = [
    'fileSuffix'     => '.jpg',
    'timeInSecs'     => 0,
    'width'          => '',
    'height'         => '',
  ];

  public $defaultGifOptions = [
    'videoEncoder'      => 'gif',
    'fileSuffix'        => '',
    'fileFormat'        => '',
    'videoCodec'        => '',
    'videoCodecOptions' => '',
  ];

  // Public Methods
  // =========================================================================

  public function init(){
    parent::init();
  }

  public function rules(){
    return [
      ['ffmpegPath', 'string'],
      ['ffmpegPath', 'required'],
      ['ffprobePath', 'string'],
      ['ffprobePath', 'required'],
      ['ffprobeOptions', 'string'],
      ['ffprobeOptions', 'safe'],
      ['videoPath', 'string'],
      ['videoPath', 'required'],
      ['videoPaths', ArrayValidator::class],
      ['videoPaths', 'required'],
      ['videoUrls', ArrayValidator::class],
      ['videoUrls', 'required'],
      ['useHashedNames', 'boolean'],
      ['useHashedNames', 'default', 'value' => false],
      ['videoEncoders', 'required'],
      ['defaultVideoOptions', 'required'],
      ['defaultThumbnailOptions', 'required'],
    ];
  }
}
