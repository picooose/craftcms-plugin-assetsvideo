<?php

namespace apstudio\assetsvideo;

use apstudio\assetsvideo\variables\AssetsVideoVariable;
use apstudio\assetsvideo\models\Settings;

use Craft;
use craft\base\Plugin;
use craft\elements\Asset;
use craft\events\ModelEvent;
use craft\events\AssetThumbEvent;
use craft\events\RegisterCacheOptionsEvent;
use craft\helpers\Assets as AssetsHelper;
use craft\helpers\FileHelper;
use craft\services\Assets;
use craft\utilities\ClearCaches;
use craft\web\twig\variables\CraftVariable;
use yii\base\ErrorException;
use yii\base\Event;

class AssetsVideo extends Plugin{
  // Static Properties
  // =========================================================================

  public static $plugin;

  // Public Methods
  // =========================================================================

  public function init(){
    parent::init();

    self::$plugin = $this;

    // Add in our Craft components
    $this->addComponents();
    // Install our global event handlers
    $this->installEventHandlers();
  }

  /**
   * Clear all the caches!
   */
  public function clearAllCaches(){
    $videoPaths = AssetsVideo::$plugin->getSettings()->videoPaths;
    foreach ($videoPaths as $key => $value) {
      $dir = Craft::getAlias($value);
      try {
        FileHelper::clearDirectory($dir);
        Craft::info(
          Craft::t(
            'assetsvideo',
            '{name} cache directory cleared',
            ['name' => $key]
          ),
          __METHOD__
        );
      } catch (ErrorException $e) {
        // the directory doesn't exist
        Craft::error($e->getMessage(), __METHOD__);
      }
    }
  }

  // Protected Methods
  // =========================================================================

  protected function createSettingsModel(){
    return new Settings();
  }

  protected function addComponents(){
    // Register our variables
    Event::on(
      CraftVariable::class,
      CraftVariable::EVENT_INIT,
      function (Event $event) {
        /** @var CraftVariable $variable */
        $variable = $event->sender;
        $variable->set('video', AssetsVideoVariable::class);
      }
    );
  }

  protected function installEventHandlers(){
    //Handler: Assets::EVENT_AFTER_SAVE
    Event::on(
      Asset::class,
      Asset::EVENT_AFTER_SAVE,
      function (ModelEvent $event) {
        if($event->isNew){
          $asset = Asset::findOne($event->sender->id);
          if ($asset && $asset->kind === Asset::KIND_VIDEO){
            AssetsVideo::$plugin->process->saveVideoDimensions($event);
          }
        }
      }
    );

    // Handler: Assets::EVENT_GET_THUMB_PATH
    Event::on(
      Assets::class,
      Assets::EVENT_GET_THUMB_PATH,
      function (AssetThumbEvent $event) {
        Craft::debug(
          'Assets::EVENT_GET_THUMB_PATH',
          __METHOD__
        );

        $asset = $event->asset;
        if (AssetsHelper::getFileKindByExtension($asset->filename) === Asset::KIND_VIDEO) {
          $event->path = AssetsVideo::$plugin->process->handleGetAssetThumbPath($event);
        }
      }
    );

    // Add the Transcode path to the list of things the Clear Caches tool can delete.
    Event::on(
      ClearCaches::class,
      ClearCaches::EVENT_REGISTER_CACHE_OPTIONS,
      function (RegisterCacheOptionsEvent $event) {
        $event->options[] = [
          'key' => 'apstudio-assetsvideo',
          'label' => 'Assets Video caches',
          'action' => [$this, 'clearAllCaches']
        ];
      }
    );
  }
}
