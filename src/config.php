<?php

return [

  // The path to the ffmpeg binary
  'ffmpegPath' => '/usr/bin/ffmpeg',

  // The path to the ffprobe binary
  'ffprobePath' => '/usr/bin/ffprobe',

  // The options to use for ffprobe
  'ffprobeOptions' => '-v quiet -print_format json -show_format -show_streams',

  // The path where the transcoded videos are stored; must have a trailing /
  // Yii2 aliases are supported here
  'videoPaths' => [
    'default'   => '@webroot/video/',
    'video'     => '@webroot/video/video/',
    'thumbnail' => '@webroot/video/thumbnail/',
    'gif'       => '@webroot/video/gif/',
  ],

  // The URL where the transcoded videos are stored; must have a trailing /
  // Yii2 aliases are supported here
  'videoUrls' => [
    'default'   => '@web/video/',
    'video'     => '@web/video/video/',
    'thumbnail' => '@web/video/thumbnail/',
    'gif'       => '@web/video/gif/',
  ],

  // Use a md5 hash for the filenames instead of parameterized naming
  'useHashedNames'    => true,
  'hashedNamesLength' => 10,

  // Preset video encoders
  'videoEncoders' => [
    'h264' => [
      'fileSuffix'        => '.mp4',
      'fileFormat'        => 'mp4',
      'videoCodec'        => 'libx264',
      'videoCodecOptions' => '-vprofile high -preset slow -crf 22',
      'audioCodec'        => 'libfdk_aac',
      'audioCodecOptions' => '-async 1000',
    ],
    'webm' => [
      'fileSuffix'        => '.webm',
      'fileFormat'        => 'webm',
      'videoCodec'        => 'libvpx',
      'videoCodecOptions' => '-quality good -cpu-used 0',
      'audioCodec'        => 'libvorbis',
      'audioCodecOptions' => '-async 1000',
    ],
    'gif' => [
      'fileSuffix'        => '.mp4',
      'fileFormat'        => 'mp4',
      'videoCodec'        => 'libx264',
      'videoCodecOptions' => '-pix_fmt yuv420p -movflags +faststart -filter:v crop=\'floor(in_w/2)*2:floor(in_h/2)*2\' ',
    ],
  ],

  // Default options for encoded videos
  'defaultVideoOptions' => [
    // Video settings
    'videoEncoder'    => 'h264',
    'videoBitRate'    => '800k',
    'videoFrameRate'  => 15,
    // Audio settings
    'audioBitRate'    => '',
    'audioSampleRate' => '',
    'audioChannels'   => '',
    // Spatial settings
    'width'           => '',
    'height'          => '',
  ],

  // Default options for video thumbnails
  'defaultThumbnailOptions' => [
    'fileSuffix'     => '.jpg',
    'timeInSecs'     => 0,
    'width'          => '',
    'height'         => ''
  ],

  // Default options for Gif encoding
  'defaultGifOptions' => [
    'videoEncoder'      => 'gif',
    'fileSuffix'        => '.mp4',
    'fileFormat'        => 'gif',
    'videoCodec'        => 'libx264',
    'videoCodecOptions' => '-pix_fmt yuv420p -movflags +faststart -filter:v crop=\'floor(in_w/2)*2:floor(in_h/2)*2\' ',
  ],
];
