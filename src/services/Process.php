<?php

namespace apstudio\assetsvideo\services;

use apstudio\assetsvideo\AssetsVideo;

use Craft;
use craft\base\Component;
use craft\elements\Asset;
use craft\events\AssetThumbEvent;
use craft\helpers\FileHelper;
use craft\helpers\Json as JsonHelper;
use craft\volumes\Local;

use yii\base\Exception;
use yii\validators\UrlValidator;

use mikehaertl\shellcommand\Command as ShellCommand;
use yii\base\InvalidConfigException;

class Process extends Component{
  // Constants
  // =========================================================================

  // Suffixes to add to the generated filename params
  const SUFFIX_MAP = [
    'videoFrameRate' => 'fps',
    'videoBitRate'   => 'bps',
    'audioBitRate'   => 'bps',
    'audioChannels'  => 'c',
    'height'         => 'h',
    'width'          => 'w',
    'timeInSecs'     => 's',
  ];

  // Params that should be excluded from being part of the generated filename
  const EXCLUDE_PARAMS = [
    'videoEncoder',
    'audioEncoder',
    'fileSuffix',
    'sharpen',
  ];

  // Mappings for getFileInfo() summary values
  const INFO_SUMMARY = [
    'format' => [
      'filename' => 'filename',
      'duration' => 'duration',
      'size'     => 'size',
    ],
    'video' => [
      'codec_name'     => 'videoEncoder',
      'bit_rate'       => 'videoBitRate',
      'avg_frame_rate' => 'videoFrameRate',
      'height'         => 'height',
      'width'          => 'width',
    ],
  ];

  // Public Methods
  // =========================================================================

  public function getVideoUrl($filePath, $videoOptions): string{
    $result        = '';
    $settings      = AssetsVideo::$plugin->getSettings();
    $destVideoPath = $this->getDestPath($filePath);
    $filePath      = $this->getAssetPath($filePath);

    if (!empty($filePath)) {
      $videoOptions = $this->coalesceOptions('defaultVideoOptions', $videoOptions);

      // Get the video encoder presets to use
      $videoEncoders = $settings['videoEncoders'];
      $thisEncoder = $videoEncoders[$videoOptions['videoEncoder']];

      $videoOptions['fileSuffix'] = $thisEncoder['fileSuffix'];

      // Build the basic command for ffmpeg
      $ffmpegCmd = $settings['ffmpegPath']
        . ' -i ' . escapeshellarg($filePath)
        . ' -vcodec ' . $thisEncoder['videoCodec']
        . ' ' . $thisEncoder['videoCodecOptions']
        . ' -bufsize 1000k'
        . ' -threads 0';

      // Set the framerate if desired
      if (!empty($videoOptions['videoFrameRate'])) {
        $ffmpegCmd .= ' -r ' . $videoOptions['videoFrameRate'];
      }

      // Set the bitrate if desired
      if (!empty($videoOptions['videoBitRate'])) {
        $ffmpegCmd .= ' -b:v ' . $videoOptions['videoBitRate'] . ' -maxrate ' . $videoOptions['videoBitRate'];
      }

      // Create the directory if it isn't there already
      if (!is_dir($destVideoPath)) {
        try {
          FileHelper::createDirectory($destVideoPath);
        } catch (Exception $e) {
          Craft::error($e->getMessage(), __METHOD__);
        }
      }

      $destVideoFile = $this->getFilename($filePath, $videoOptions);

      // File to store the video encoding progress in
      $progressFile = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $destVideoFile . '.progress';

      // Assemble the destination path and final ffmpeg command
      $destVideoPath .= $destVideoFile;
      $ffmpegCmd .= ' -f '
        . $thisEncoder['fileFormat']
        . ' -y ' . escapeshellarg($destVideoPath)
        . ' 1> ' . $progressFile . ' 2>&1 & echo $!';

      // Make sure there isn't a lockfile for this video already
      $lockFile = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $destVideoFile . '.lock';
      $oldPid = @file_get_contents($lockFile);
      if ($oldPid !== false) {
        exec("ps $oldPid", $ProcessState);
        if (\count($ProcessState) >= 2) {
          return $result;
        }
        // It's finished transcoding, so delete the lockfile and progress file
        @unlink($lockFile);
        @unlink($progressFile);
      }

      // If the video file already exists and hasn't been modified, return it.  Otherwise, start it transcoding
      if (file_exists($destVideoPath) && (@filemtime($destVideoPath) >= @filemtime($filePath))) {
        $result = Craft::getAlias($settings['videoUrls']['default']) . $destVideoFile;
      } else {
        // Kick off the transcoding
        $pid = $this->executeShellCommand($ffmpegCmd);
        Craft::info($ffmpegCmd . "\nffmpeg PID: " . $pid, __METHOD__);

        // Create a lockfile in tmp
        file_put_contents($lockFile, $pid);
      }
    }

    return $result;
  }

  public function getVideoThumbnailUrl($filePath, $thumbnailOptions, $generate = true, $asPath = false){

    $result            = null;
    $settings          = AssetsVideo::$plugin->getSettings();
    $destThumbnailUrl  = $this->getDestUrl($filePath);
    $destThumbnailPath = $this->getDestPath($filePath);
    $filePath          = $this->getAssetPath($filePath);

    if (!empty($filePath)) {
      $thumbnailOptions = $this->coalesceOptions('defaultThumbnailOptions', $thumbnailOptions);

      // Build the basic command for ffmpeg
      $ffmpegCmd = $settings['ffmpegPath']
        . ' -i ' . escapeshellarg($filePath)
        . ' -vcodec mjpeg'
        . ' -vframes 1';


      // Set the timecode to get the thumbnail from if desired
      if (!empty($thumbnailOptions['timeInSecs'])) {
        $timeCode = gmdate('H:i:s', $thumbnailOptions['timeInSecs']);
        $ffmpegCmd .= ' -ss ' . $timeCode . '.00';
      }

      // Create the directory if it isn't there already
      if (!is_dir($destThumbnailPath)) {
        try {
          FileHelper::createDirectory($destThumbnailPath);
        } catch (Exception $e) {
          Craft::error($e->getMessage(), __METHOD__);
        }
      }

      $destThumbnailFile = $this->getFilename($filePath, $thumbnailOptions);

      // Assemble the destination path and final ffmpeg command
      $destThumbnailPath .= $destThumbnailFile;

      $ffmpegCmd .= ' -f image2 -y ' . escapeshellarg($destThumbnailPath) . ' >/dev/null 2>/dev/null &';

      // If the thumbnail file already exists, return it.  Otherwise, generate it and return it
      if (!file_exists($destThumbnailPath)) {
        if ($generate) {
          $shellOutput = $this->executeShellCommand($ffmpegCmd);
          Craft::info($ffmpegCmd, __METHOD__);
        }
      }

      // Return either a path or a URL
      if ($asPath) {
        $result = $destThumbnailPath;
      } else {
        $result = $destThumbnailUrl . $destThumbnailFile;
      }
    }

    return $result;
  }

  public function getGifUrl($filePath, $gifOptions): string{
    $result        = '';
    $settings      = AssetsVideo::$plugin->getSettings();
    $destVideoPath = $this->getDestPath($filePath);
    $filePath      = $this->getAssetPath($filePath);

    if (!empty($filePath)) {
      // Options
      $gifOptions = $this->coalesceOptions('defaultGifOptions', $gifOptions);

      // Get the video encoder presets to use
      $videoEncoders = $settings['videoEncoders'];
      $thisEncoder = $videoEncoders[$gifOptions['videoEncoder']];
      $gifOptions['fileSuffix'] = $thisEncoder['fileSuffix'];

      // Build the basic command for ffmpeg
      $ffmpegCmd = $settings['ffmpegPath']
        . ' -f gif'
        . ' -i ' . escapeshellarg($filePath)
        . ' -vcodec ' . $thisEncoder['videoCodec']
        . ' ' . $thisEncoder['videoCodecOptions'];


      // Create the directory if it isn't there already
      if (!is_dir($destVideoPath)) {
        try {
          FileHelper::createDirectory($destVideoPath);
        } catch (Exception $e) {
          Craft::error($e->getMessage(), __METHOD__);
        }
      }

      $destVideoFile = $this->getFilename($filePath, $gifOptions);

      // File to store the video encoding progress in
      $progressFile = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $destVideoFile . '.progress';

      // Assemble the destination path and final ffmpeg command
      $destVideoPath .= $destVideoFile;
      $ffmpegCmd .= ' '
        . ' -y ' . escapeshellarg($destVideoPath)
        . ' 1> ' . $progressFile . ' 2>&1 & echo $!';

      // Make sure there isn't a lockfile for this video already
      $lockFile = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $destVideoFile . '.lock';
      $oldPid = @file_get_contents($lockFile);
      if ($oldPid !== false) {
        exec("ps $oldPid", $ProcessState);
        if (\count($ProcessState) >= 2) {
          return $result;
        }
        // It's finished transcoding, so delete the lockfile and progress file
        @unlink($lockFile);
        @unlink($progressFile);
      }

      // If the video file already exists and hasn't been modified, return it.  Otherwise, start it transcoding
      if (file_exists($destVideoPath) && (@filemtime($destVideoPath) >= @filemtime($filePath))) {
        $result = Craft::getAlias($settings['videoUrls']['default']) . $destVideoFile;
      } else {
        // Kick off the transcoding
        $pid = $this->executeShellCommand($ffmpegCmd);
        Craft::info($ffmpegCmd . "\nffmpeg PID: " . $pid, __METHOD__);

        // Create a lockfile in tmp
        file_put_contents($lockFile, $pid);
      }
    }

    return $result;
  }


  public function getFileInfo($filePath, $summary = false): array{

    $result   = null;
    $settings = AssetsVideo::$plugin->getSettings();
    $filePath = $this->getAssetPath($filePath);

    if (!empty($filePath)) {
      // Build the basic command for ffprobe
      $ffprobeOptions = $settings['ffprobeOptions'];
      $ffprobeCmd = $settings['ffprobePath']
        . ' ' . $ffprobeOptions
        . ' ' . escapeshellarg($filePath);

      $shellOutput = $this->executeShellCommand($ffprobeCmd);
      Craft::info($ffprobeCmd, __METHOD__);
      $result = JsonHelper::decodeIfJson($shellOutput, true);
      Craft::info(print_r($result, true), __METHOD__);

      // Trim down the arrays to just a summary
      if ($summary && !empty($result)) {
        $summaryResult = [];
        foreach ($result as $topLevelKey => $topLevelValue) {
          switch ($topLevelKey) {
              // Format info
            case 'format':
              foreach (self::INFO_SUMMARY['format'] as $settingKey => $settingValue) {
                if (!empty($topLevelValue[$settingKey])) {
                  $summaryResult[$settingValue] = $topLevelValue[$settingKey];
                }
              }
              break;
              // Stream info
            case 'streams':
              foreach ($topLevelValue as $stream) {
                $infoSummaryType = $stream['codec_type'];
                foreach (self::INFO_SUMMARY[$infoSummaryType] as $settingKey => $settingValue) {
                  if (!empty($stream[$settingKey])) {
                    $summaryResult[$settingValue] = $stream[$settingKey];
                  }
                }
              }
              break;
              // Unknown info
            default:
              break;
          }
        }
        // Handle cases where the framerate is returned as XX/YY
        if (
          !empty($summaryResult['videoFrameRate'])
          && (strpos($summaryResult['videoFrameRate'], '/') !== false)
        ) {
          $parts = explode('/', $summaryResult['videoFrameRate']);
          $summaryResult['videoFrameRate'] = (float) $parts[0] / (float) $parts[1];
        }
        $result = $summaryResult;
      }
    }

    return $result;
  }

  public function getVideoDimensions($filePath): array{
    $result = [];
    $details = $this->getFileInfo($filePath, true);

    if (!empty($details) && isset($details['width']) && isset($details['height'])) {
      $result = [
        'width'  => $details['width'],
        'height' => $details['height'],
        'ratio'  => $details['width'] / $details['height']
      ];
    }

    return $result;
  }

  public function saveVideoDimensions($event){
    $result = false;
    $asset  = Asset::findOne($event->sender->id);

    if ($asset) {
      $infos = $this->getVideoDimensions($asset);

      if(isset($infos['width']) && isset($infos['height'])){
        $asset->setAttributes([
          'width' => $infos['width'],
          'height' => $infos['height'],
        ]);

        $result = Craft::$app->getElements()->saveElement($asset);
      }
    }

    return $result;
  }

  public function getVideoFilename($filePath, $videoOptions): string{
    $settings = AssetsVideo::$plugin->getSettings();
    $videoOptions = $this->coalesceOptions('defaultVideoOptions', $videoOptions);

    // Get the video encoder presets to use
    $videoEncoders              = $settings['videoEncoders'];
    $thisEncoder                = $videoEncoders[$videoOptions['videoEncoder']];

    $videoOptions['fileSuffix'] = $thisEncoder['fileSuffix'];

    return $this->getFilename($filePath, $videoOptions);
  }


  public function getGifFilename($filePath, $gifOptions): string{
    $settings = AssetsVideo::$plugin->getSettings();
    $gifOptions = $this->coalesceOptions('defaultGifOptions', $gifOptions);

    // Get the video encoder presets to use
    $videoEncoders            = $settings['videoEncoders'];
    $thisEncoder              = $videoEncoders[$gifOptions['videoEncoder']];

    $gifOptions['fileSuffix'] = $thisEncoder['fileSuffix'];

    return $this->getFilename($filePath, $gifOptions);
  }

  public function handleGetAssetThumbPath(AssetThumbEvent $event){
    $options = [
      'width' => $event->width,
      'height' => $event->height,
    ];
    $thumbPath = $this->getVideoThumbnailUrl($event->asset, $options, $event->generate, true);

    return $thumbPath;
  }

  // Protected Methods
  // =========================================================================

  protected function getFilename($filePath, $options): string{
    $settings = AssetsVideo::$plugin->getSettings();
    $filePath = $this->getAssetPath($filePath);

    $validator = new UrlValidator();
    $error = '';
    if ($validator->validate($filePath, $error)) {
      $urlParts = parse_url($filePath);
      $pathParts = pathinfo($urlParts['path']);
    } else {
      $pathParts = pathinfo($filePath);
    }
    $fileName = $pathParts['filename'];

    // Add our options to the file name
    foreach ($options as $key => $value) {
      if (!empty($value)) {
        $suffix = '';
        if (!empty(self::SUFFIX_MAP[$key])) {
          $suffix = self::SUFFIX_MAP[$key];
        }
        if (\is_bool($value)) {
          $value = $value ? $key : 'no' . $key;
        }
        if (!\in_array($key, self::EXCLUDE_PARAMS, true)) {
          $fileName .= '_' . $value . $suffix;
        }
      }
    }
    // See if we should use a hash instead
    if ($settings['useHashedNames']) {
      $fileName = substr(md5($fileName), 0, $settings['hashedNamesLength']);
    }
    $fileName .= $options['fileSuffix'];

    return $fileName;
  }

  protected function getDestPath($filePath): string{
    $settings = AssetsVideo::$plugin->getSettings();
    $path     = $settings['videoPaths']['default'];

    // If we're passed an Asset, we store append its id to the path
    if (\is_object($filePath) && ($filePath instanceof Asset)) {
      $path .= $filePath->id . '/';
    }

    return Craft::getAlias($path);
  }

  protected function getDestUrl($filePath): string{
    $settings = AssetsVideo::$plugin->getSettings();
    $url     = $settings['videoUrls']['default'];

    // If we're passed an Asset, we store append its id to the url
    if (\is_object($filePath) && ($filePath instanceof Asset)) {
      $url .= $filePath->id . '/';
    }

    return Craft::getAlias($url);
  }

  protected function getAssetPath($filePath): string{
    // If we're passed an Asset, extract the path from it
    if (\is_object($filePath) && ($filePath instanceof Asset)) {
      /** @var Asset $asset */
      $asset = $filePath;
      $assetVolume = null;
      try {
        $assetVolume = $asset->getVolume();
      } catch (InvalidConfigException $e) {
        Craft::error($e->getMessage(), __METHOD__);
      }

      if ($assetVolume) {
        // If it's local, get a path to the file
        if ($assetVolume instanceof Local) {
          $sourcePath = rtrim($assetVolume->path, DIRECTORY_SEPARATOR);
          $sourcePath .= '' === $sourcePath ? '' : DIRECTORY_SEPARATOR;
          $folderPath = '';
          try {
            $folderPath = rtrim($asset->getFolder()->path, DIRECTORY_SEPARATOR);
          } catch (InvalidConfigException $e) {
            Craft::error($e->getMessage(), __METHOD__);
          }
          $folderPath .= '' === $folderPath ? '' : DIRECTORY_SEPARATOR;

          $filePath = $sourcePath . $folderPath . $asset->filename;
        } else {
          // Otherwise, get a URL
          $filePath = $asset->getUrl();
        }
      }
    }

    $filePath = Craft::getAlias($filePath);

    // Make sure that $filePath is either an existing file, or a valid URL
    if (!file_exists($filePath)) {
      $validator = new UrlValidator();
      $error = '';
      if (!$validator->validate($filePath, $error)) {
        Craft::error($error, __METHOD__);
        $filePath = '';
      }
    }

    return $filePath;
  }


  // Protected Methods
  // =========================================================================

  protected function coalesceOptions($defaultName, $options): array{
    // Default options
    $settings = AssetsVideo::$plugin->getSettings();
    $defaultOptions = $settings[$defaultName];

    // Coalesce the passed in $options with the $defaultOptions
    $options = array_merge($defaultOptions, $options);

    return $options;
  }


  protected function executeShellCommand(string $command): string{
    // Create the shell command
    $shellCommand = new ShellCommand();
    $shellCommand->setCommand($command);

    // If we don't have proc_open, maybe we've got exec
    if (!\function_exists('proc_open') && \function_exists('exec')) {
      $shellCommand->useExec = true;
    }

    // Return the result of the command's output or error
    if ($shellCommand->execute()) {
      $result = $shellCommand->getOutput();
    } else {
      $result = $shellCommand->getError();
    }

    return $result;
  }
}
